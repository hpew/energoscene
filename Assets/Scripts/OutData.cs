﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OutData : MonoBehaviour
{

	public VRTK.VRTK_InteractObjectHighlighter highlighter;

	public Animator anim;
	public bool isRotation = false;
	public bool isBig = false;

	public string name;

	public void Awake()
	{

		highlighter = GetComponent<VRTK.VRTK_InteractObjectHighlighter>();

		highlighter.InteractObjectHighlighterHighlighted += OutDataOnTheUI;
		highlighter.InteractObjectHighlighterUnhighlighted += CloseTab;
	}

	
	public void OutDataOnTheUI(object sender, VRTK.InteractObjectHighlighterEventArgs e)
	{
		if (isRotation)
		{

			Vector3 test = new Vector3(e.affectingObject.transform.position.x, anim.GetComponent<RectTransform>().position.y, e.affectingObject.transform.position.z);
			Vector3 relativePos = test - anim.GetComponent<RectTransform>().position;

			Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
			anim.GetComponent<RectTransform>().rotation = rotation;

		}

		if (isBig)
		{
			e.affectingObject.GetComponentInChildren<ObjectData>().SetText(name);
			e.affectingObject.GetComponentInChildren<ObjectData>().SetAnimation(true);

			if (this.tag == "Emission")
			{

				this.GetComponentInChildren<Light>().enabled = true;
				//	print(this.GetComponent<MeshRenderer>().material);

				if (!this.GetComponent<MeshRenderer>().material.IsKeywordEnabled("_EMISSION"))
				{
					print("ENABLED");
					this.GetComponent<MeshRenderer>().material.EnableKeyword("_EMISSION");
				}



				//	this.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.white * Mathf.LinearToGammaSpace(10.0f));
				//print(this.GetComponent<MeshRenderer>().material.color);
			}


			return;
		}
		
	
		anim.SetBool("In", true);
	}


	

	public void CloseTab(object sender, VRTK.InteractObjectHighlighterEventArgs e)
	{

		if (isBig)
		{
			e.affectingObject.GetComponentInChildren<ObjectData>().SetAnimation(false);
			if (this.tag == "Emission")
			{
				this.GetComponentInChildren<Light>().enabled = false;

				if (this.GetComponent<MeshRenderer>().material.IsKeywordEnabled("_EMISSION"))
				{
					print("DISABLED");
					this.GetComponent<MeshRenderer>().material.DisableKeyword("_EMISSION");
				}
				//this.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
			}
			return;
		}
		anim.SetBool("In", false);
		
	}
}
