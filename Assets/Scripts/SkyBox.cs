﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBox : MonoBehaviour
{
	public Material skyBoxMaterial;
	public Light[] lightsLam;


	LensFlare flare;
	Light directionalLight;

	public float fullDay = 120f; // сколько длиться день, в секундах
	[Range(0, 1)] public float currentTime;

	float h, m, s, oldS = 0;
	float oldH = 12;
	float exposure = 0.5f;
	int timer;
	int lampTimer;
	// Start is called before the first frame update
	void Start()
	{
		flare = GetComponent<LensFlare>();
		directionalLight = GetComponent<Light>();

		directionalLight.intensity = 1.4f;
		flare.brightness = 1.0f;

		skyBoxMaterial.SetFloat("_Exposure", 0.5f);

		timer = (int)fullDay / 24;
		currentTime = 0.5f;
	}

	// Update is called once per frame
	void Update()
	{
		TimeMain();
		currentTime += Time.deltaTime / fullDay;

		if (currentTime >= 1)
		{
			currentTime = 0;
			directionalLight.intensity = 0.0f;
			flare.brightness = 0.0f;
			oldH = 0;
			exposure = 0.0f;
			skyBoxMaterial.SetFloat("_Exposure", exposure);
		}
		else if (currentTime < 0)
		{
			currentTime = 0;
			directionalLight.intensity = 0.0f;
			flare.brightness = 0.0f;
			oldH = 0;
			exposure = 0.0f;
			skyBoxMaterial.SetFloat("_Exposure", exposure);
		}

		


	}

	void TimeMain()
	{
		 h = 24 * currentTime;

		if (h >= 21 && h <= 24 && lightsLam[0].enabled == false)
		{
		
			foreach (var item in lightsLam)
			{
				item.enabled = true;
			}
		}
		else if (h >= 8 && h < 21 && lightsLam[0].enabled == true)
		{
			foreach (var item in lightsLam)
			{
				item.enabled = false;
			}
		}



		if (currentTime <= 0.5f && (h - oldH) >= 0.019f)
		{
			if (directionalLight.intensity > 1.4f) directionalLight.intensity = 1.4f;
			if (flare.brightness > 1.0f) flare.brightness = 1.0f;
			if (exposure > 1.0f)
			{
				exposure = 0.0f;
				skyBoxMaterial.SetFloat("_Exposure", exposure);
			}
			oldH = h;

			directionalLight.intensity += 1.4f / fullDay;
			flare.brightness += 1.0f / fullDay;
			exposure += 0.5f / fullDay;
			skyBoxMaterial.SetFloat("_Exposure", exposure);


		}
		else if (currentTime >= 0.5f && (h - oldH) >= 0.019f)
		{
			if (directionalLight.intensity < 0) directionalLight.intensity = 0;
			if (flare.brightness < 0.0f) flare.brightness = 0.0f;
			if (exposure < 0.0f)
			{
				exposure = 0.0f;
				skyBoxMaterial.SetFloat("_Exposure", exposure);
			}
			oldH = h;
			directionalLight.intensity -= 1.4f / fullDay;
			flare.brightness -= 1.0f / fullDay;
			exposure -= 0.5f / fullDay;
			skyBoxMaterial.SetFloat("_Exposure", exposure);
		}

	}
}
