﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelSoundPlay : MonoBehaviour
{

	public AudioSource audio;

	public AudioClip clip;


	public void Play()
	{
		
		audio.clip = clip;

		audio.Play();
	}
}
